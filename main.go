package main

import (
	"errors"
	"fmt"
	"strconv"
	"sync"
	"sync/atomic"

	//	"github.com/golang/glog"
	zmq "github.com/pebbe/zmq4"
)

var (
	FULL_CONNECTION_POOL  = errors.New("Connection pool reached max size")
	EMPTY_CONNECTION_POOL = errors.New("Connection pool reached min size")
)

type BrokerConnection interface {
}

type Producer interface {
	Send(msg []byte) error
	Close() error
}

type Consumer interface {
	Receive(offset int) ([]byte, error)
	Close() error
}

type Broker interface {
	Listen(host string, port int) error
}

type ProducerConnection struct {
	Context *zmq.Context
	Socket  *zmq.Socket
}

type ProducerImpl struct {
	lock        sync.Mutex
	Pool        []*ProducerConnection
	poolSize    uint64
	maxSize     uint64
	bufferQueue chan []byte
}

func (p *ProducerImpl) pushConn(conn *ProducerConnection) error {
	p.lock.Lock()
	defer p.lock.Unlock()

	if atomic.LoadUint64(&p.poolSize) == p.maxSize {
		return FULL_CONNECTION_POOL
	}
	p.Pool = append(p.Pool, conn)

	return nil
}

func (p *ProducerImpl) popConn() (*ProducerConnection, error) {
	p.lock.Lock()
	defer p.lock.Unlock()

	length := atomic.LoadUint64(&p.poolSize)
	if length == 0 {
		return nil, EMPTY_CONNECTION_POOL
	}

	conn := p.Pool[length-1]
	p.Pool = p.Pool[:length-1]
	atomic.StoreUint64(&p.poolSize, length-1)

	return conn, nil
}

func (p *ProducerImpl) Close() error {
	for i := range p.Pool {
		p.Pool[i].Context.Term()
		p.Pool[i].Socket.Close()
	}

	return nil
}

func (p *ProducerImpl) Send(msg []byte) error {

	return nil
}

func NewProducer(host string, port, poolSize int) (*ProducerImpl, error) {
	producer := ProducerImpl{
		maxSize:  uint64(poolSize),
		poolSize: uint64(poolSize),
		Pool:     make([]*ProducerConnection, poolSize),
	}

	for i := 0; i < poolSize; i++ {
		ctx, err := zmq.NewContext()
		if err != nil {
			return nil, err
		}

		socket, err := ctx.NewSocket(zmq.PUB)
		if err != nil {
			return nil, err
		}

		if err := socket.Bind(makeConnectionAddr(host, port)); err != nil {
			return nil, err
		}

		producer.pushConn(&ProducerConnection{
			Context: ctx,
			Socket:  socket,
		})
	}

	return &producer, nil
}

func makeConnectionAddr(host string, port int) string {
	return fmt.Sprintf("tcp://%v:%v", host, strconv.Itoa(port))
}

func main() {
	producer, _ := NewProducer("", 1, 5)

	fmt.Println(producer)
	fmt.Println(producer.popConn())
	fmt.Println(producer)
}
